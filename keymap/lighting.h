#pragma once

extern rgb_config_t rgb_matrix_config;

int blink_index;
const uint8_t* blink_color;
int blink_count;
bool blink_active;
uint32_t blink_timer;

void write_hsv_matrix(void);
void hsv_matrix_set_color(int, const uint8_t[3]);
void set_layer_color(int);
void blink_key(int, int, const uint8_t[3]);
