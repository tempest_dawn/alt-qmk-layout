#include "keymap.h"
#include "lighting.h"
#include "lighting_layers.h"
#include "print.h"

void hsv_matrix_set_color(int index, const uint8_t hsv_color[3]) {
    if (index >= DRIVER_LED_TOTAL)
        return;

    HSV hsv = {
        .h = hsv_color[0],
        .s = hsv_color[1],
        .v = hsv_color[2],
    };

    RGB rgb = hsv_to_rgb(hsv);
    float f = (float)rgb_matrix_config.hsv.v / UINT8_MAX;
    rgb_matrix_set_color(index, f * rgb.r, f * rgb.g, f * rgb.b);
}

void set_layer_color(int layer) {
    int skip_index = -1;

    // Blink handling
    if (blink_count > 0) {
        skip_index = blink_index;
        if (blink_active) {
            hsv_matrix_set_color(blink_index, blink_color);
        } else {
            hsv_matrix_set_color(blink_index, (uint8_t[3]) {0, 0, 0});
        }

        if(timer_elapsed32(blink_timer) > 300) {
            blink_timer = timer_read32();
            blink_active = !blink_active;

            if (blink_active) {
                blink_count--;
            }
        }
    } else {
        blink_active = false;
        blink_timer = 0;
        blink_index = 0;
    }

    for (int i = 0; i < DRIVER_LED_TOTAL; i++) {
        if (i != skip_index) {
            HSV hsv = {
                .h = pgm_read_byte(&ledmap[layer][i][0]),
                .s = pgm_read_byte(&ledmap[layer][i][1]),
                .v = pgm_read_byte(&ledmap[layer][i][2]),
            };

            // Regular layer handling
            if(hsv.h || hsv.s || hsv.v) {
                hsv_matrix_set_color(i, ledmap[layer][i]);
            } else if (
                layer == _GAME_LAYER
            ) {
                // Allow lights to be blank
                hsv_matrix_set_color(i, (uint8_t[3]) {0, 0, 0});
            }
        };
    }

}

void blink_key(int key_index, int count, const uint8_t color[3]) {
    blink_active = true;
    blink_timer = timer_read32();
    blink_count = count;
    blink_index = key_index;
    blink_color = color;
}
