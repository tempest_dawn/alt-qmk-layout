#pragma once

#include QMK_KEYBOARD_H

#define C_TRA1 {HSV_CYAN}
#define C_TRA2 {HSV_MAGENTA}
#define C_TRA3 {HSV_WHITE}

#define C_PRIM {HSV_PURPLE}
#define C_SECO {HSV_GOLD}
#define C_MODI {HSV_MAGENTA}

#define C_TALI {HSV_RED}
#define C_TAL2 {HSV_YELLOW}
#define C_TAL3 {HSV_GREEN}

#define C_RED {HSV_RED}
#define C_GRN {HSV_GREEN}
#define C_ORNG {HSV_ORANGE}

enum layout_names {
  _TYPING_LAYER=0,
  _AMBER_LAYER,
  _TRANS_LAYER,
  _GAME_LAYER,
  _FUNCTION_LAYER,
  _ALT_LAYER,
  _CAPS_LAYER,
  _LOCK_LAYER,
  _LOCK_LAYER2,
  _LOCK_LAYER3,
};

extern bool g_suspend_state;
bool disable_layer_color;
bool enable_mouse_wiggle;
bool mac_mode;

void send_alt_code(int);
