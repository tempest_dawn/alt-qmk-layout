#pragma once

//Teal and Blue
#define RGB_MATRIX_STARTUP_HUE 30 			// Default boot color
#define RGB_MATRIX_STARTUP_SAT 0 			// Default boot color
#define RGB_MATRIX_STARTUP_SPD 15 			// Used to determine the color for the modifiers
#define RGB_MATRIX_STARTUP_VAL 200

#define TAP_HOLD_CAPS_DELAY 120

#define RGB_MATRIX_STARTUP_MODE RGB_MATRIX_ALPHAS_MODS

#define TAPPING_TERM 100
