#include "keymap.h"
#include "lighting.h"

#ifdef _______
#undef _______
#endif

#define _______ {0, 0, 0}

const uint8_t PROGMEM ledmap[][DRIVER_LED_TOTAL][3] = {
    [_TYPING_LAYER] = {
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, // 14
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, // 29
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, // 43
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          C_PRIM,  _______, // 57
        _______, _______, _______,                            _______,                            _______, _______, C_PRIM,  C_PRIM,  C_PRIM,  // 66
        //UnderGlow starts at index 67
        C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,
        C_PRIM,                                                                                                                       C_PRIM,
        C_PRIM,                                                                                                                       C_PRIM,
        C_PRIM,                                                                                                                       C_PRIM,
        C_PRIM,                                                                                                                       C_PRIM,
        C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM,  C_PRIM
    },
    [_TRANS_LAYER] = {
        C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1,
        C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2,
        C_TRA3, C_TRA3, C_TRA3, C_TRA3, C_TRA3, C_TRA3, C_TRA3, C_TRA3, C_TRA3, C_TRA3, C_TRA3, C_TRA3,         C_TRA3, C_TRA3,
        C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2, C_TRA2,         C_TRA2, C_TRA2,
        C_TRA1, C_TRA1, C_TRA1,                         C_TRA1,                         C_TRA1, C_TRA1, C_TRA1, C_TRA1, C_TRA1,
        //UnderGlow
        C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,
        C_TRA2,                                                                                                                       C_TRA2,
        C_TRA2,                                                                                                                       C_TRA2,
        C_TRA2,                                                                                                                       C_TRA2,
        C_TRA2,                                                                                                                       C_TRA2,
        C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2,  C_TRA2
    },
    [_FUNCTION_LAYER] = {
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, C_TALI,  _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, C_SECO,  _______, _______, _______, _______, _______, _______,          _______, _______,
        _______, C_PRIM,  _______, _______, _______, C_PRIM,  _______, _______, _______, _______, _______, _______,          C_MODI,  _______,
        _______, _______, _______,                            _______,                            _______, _______, C_MODI,  C_MODI,  C_MODI,
        //UnderGlow
        C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,
        C_MODI,                                                                                                                       C_MODI,
        C_MODI,                                                                                                                       C_MODI,
        C_MODI,                                                                                                                       C_MODI,
        C_MODI,                                                                                                                       C_MODI,
        C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI
    },
    [_CAPS_LAYER] = {
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, C_TRA2,  _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          C_MODI,  _______,
        _______, _______, _______,                            _______,                            _______, _______, C_MODI,  C_MODI,  C_MODI,
        //UnderGlow
        C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,
        C_MODI,                                                                                                                       C_MODI,
        C_MODI,                                                                                                                       C_MODI,
        C_MODI,                                                                                                                       C_MODI,
        C_MODI,                                                                                                                       C_MODI,
        C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI,  C_MODI
    },
    [_LOCK_LAYER] = {
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______,
        _______, _______, _______,                            _______,                            _______, _______, _______, _______, _______,
        //UnderGlow
        C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,
        C_TALI,                                                                                                                       C_TALI,
        C_TALI,                                                                                                                       C_TALI,
        C_TALI,                                                                                                                       C_TALI,
        C_TALI,                                                                                                                       C_TALI,
        C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI,  C_TALI
    },
    [_LOCK_LAYER2] = {
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______,
        _______, _______, _______,                            _______,                            _______, _______, _______, _______, _______,
        //UnderGlow
        C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,
        C_TAL2,                                                                                                                       C_TAL2,
        C_TAL2,                                                                                                                       C_TAL2,
        C_TAL2,                                                                                                                       C_TAL2,
        C_TAL2,                                                                                                                       C_TAL2,
        C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2,  C_TAL2
    },
    [_LOCK_LAYER3] = {
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______,
        _______, _______, _______,                            _______,                            _______, _______, _______, _______, _______,
        //UnderGlow
        C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,
        C_TAL3,                                                                                                                       C_TAL3,
        C_TAL3,                                                                                                                       C_TAL3,
        C_TAL3,                                                                                                                       C_TAL3,
        C_TAL3,                                                                                                                       C_TAL3,
        C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3,  C_TAL3
    },
    [_GAME_LAYER] = {
        _______, C_MODI,  C_MODI,  C_SECO,  C_SECO,  C_PRIM,  _______, _______, _______, _______, _______, _______, _______, _______, _______,
        C_PRIM,  C_PRIM,  C_SECO,  C_PRIM,  C_PRIM,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        C_PRIM,  C_SECO,  C_SECO,  C_SECO,  C_PRIM,  _______, _______, _______, _______, _______, _______, _______,          _______, _______,
        C_PRIM,  C_PRIM,  _______, C_PRIM,  _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______,
        _______, _______, _______,                            C_PRIM,                             _______, _______, _______, _______, _______,
        //UnderGlow
        C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,
        C_SECO,                                                                                                                       C_SECO,
        C_SECO,                                                                                                                       C_SECO,
        C_SECO,                                                                                                                       C_SECO,
        C_SECO,                                                                                                                       C_SECO,
        C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO,  C_SECO
    },
};
