#include "keymap.h"
#include "lighting.h"
#include "print.h"

#define _TR _TRANS_LAYER
#define _AL _AMBER_LAYER
#define _LL _LOCK_LAYER
#define _L2 _LOCK_LAYER2
#define _L3 _LOCK_LAYER3
#define _GL _GAME_LAYER
#define _FL _FUNCTION_LAYER

enum alt_keycodes {
    U_T_AUTO = SAFE_RANGE, //USB Extra Port Toggle Auto Detect / Always Active
    U_T_AGCR,              //USB Toggle Automatic GCR control
    DBG_TOG,               //DEBUG Toggle On / Off
    DBG_MTRX,              //DEBUG Toggle Matrix Prints
    DBG_KBD,               //DEBUG Toggle Keyboard Prints
    DBG_MOU,               //DEBUG Toggle Mouse Prints
    MD_BOOT,               //Restart into bootloader after hold timeout
    ALT_END
};

enum ashe_keycodes {
    AD_WIND = ALT_END, // Special partial-layer
    AD_LALT, // Special partial-layer
    AD_CAPS, // Special partial-layer
    AD_DOT,
    AD_UASH, // Æ: Upper-case ash
    AD_LASH, // æ: Lower-case ash
    AD_LOCK, // Lock screen and toggle layer
    AD_MODE,
    AD_LGUI,
    AD_CTRL,
};

#define KC_NEXT KC_MEDIA_NEXT_TRACK
#define KC_PREV KC_MEDIA_PREV_TRACK

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /*
    [DEFAULT] = LAYOUT(
        KC_ESC,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,     KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC, KC_DEL,
        KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,     KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC, KC_BSLS, KC_HOME,
        KC_CAPS, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,     KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,          KC_ENT,  KC_PGUP,
        KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,     KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT,          KC_UP,   KC_PGDN,
        KC_LCTL, KC_LGUI, KC_LALT,                             KC_SPC,                             KC_RALT, MO(1),   KC_LEFT, KC_DOWN, KC_RGHT
    ),
    */
    [_TYPING_LAYER] = LAYOUT_65_ansi_blocker(
        KC_GESC, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC, KC_DEL, \
        KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC, KC_BSLS, KC_INS, \
        AD_CAPS, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,          KC_ENT,  KC_HOME, \
        KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT,          KC_UP,   KC_END, \
        AD_CTRL, AD_LGUI, AD_LALT,                            KC_SPC,                             KC_RALT, MO(_FL), KC_LEFT, KC_DOWN, KC_RGHT  \
        // cmd, ctrl, alt in mac mode
    ),
    // Color layers
    [_AMBER_LAYER] = LAYOUT_65_ansi_blocker(
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______,                            _______,                            _______, _______, _______, _______, _______  \
    ),
    [_TRANS_LAYER] = LAYOUT_65_ansi_blocker(
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______,                            _______,                            _______, _______, _______, _______, _______  \
    ),
    [_GAME_LAYER] = LAYOUT_65_ansi_blocker(
        KC_ESC,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        KC_CAPS, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, KC_LALT,                            _______,                            _______, _______, _______, _______, _______  \
    ),
    [_FUNCTION_LAYER] = LAYOUT_65_ansi_blocker(
        KC_GRV,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  RCTL(KC_BSPC), RALT(KC_PSCR), \
        _______, RGB_SPD, RGB_VAI, RGB_SPI, RGB_HUI, TG(_LL), _______, U_T_AUTO,U_T_AGCR,_______, KC_PSCR, KC_SLCK, KC_PAUS, _______, KC_DEL, \
        _______, RGB_RMOD,RGB_VAD, RGB_MOD, RGB_HUD, TG(_GL), _______, _______, _______, AD_LOCK, _______, _______,          RCTL(KC_ENT), KC_VOLU, \
        _______, RGB_TOG, _______, _______, _______, MD_BOOT, NK_TOGG, DBG_TOG, _______, _______, RCTL(KC_SLSH), _______,    KC_PGUP, KC_VOLD, \
        KC_NLCK, _______, _______,                      KC_MEDIA_PLAY_PAUSE,                      _______, MO(_FL), RCTL(KC_LEFT), KC_PGDN, RCTL(KC_RGHT)  \
    ),
    [_ALT_LAYER] = LAYOUT_65_ansi_blocker(
        _______, _______, _______, _______, _______, _______, _______, _______, AD_DOT,  _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______,                            _______,                            _______, _______, _______, _______, _______  \
    ),
    [_CAPS_LAYER] = LAYOUT_65_ansi_blocker(
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, AD_MODE, _______, \
        _______, _______, _______, _______, _______, TG(_TR), _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          KC_VOLU, _______, \
        _______, _______, _______,                      KC_MEDIA_PLAY_PAUSE,                      _______, _______, KC_PREV, KC_VOLD, KC_NEXT  \
    ),
    [_LOCK_LAYER] = LAYOUT(
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,          XXXXXXX, XXXXXXX, \
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,          XXXXXXX, XXXXXXX, \
        MO(_L2), XXXXXXX, XXXXXXX,                            XXXXXXX,                            XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX  \
    ),
    [_LOCK_LAYER2] = LAYOUT(
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, MO(_L3), \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______,                            _______,                            _______, _______, _______, _______, _______  \
    ),
    [_LOCK_LAYER3] = LAYOUT(
        TG(_LL), _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, _______, \
        _______, _______, _______,                            _______,                            _______, _______, _______, _______, _______  \
    ),
};

static const uint8_t* mac_mode_blink_color = (uint8_t[3]) C_TRA2;
static const int mac_mode_index = 58;
static const int pc_mode_index = 59;

// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {
    // Enable or disable debugging
    debug_enable=true;
    debug_matrix=false;
    debug_keyboard=true;
    //rgb_enabled_flag = true;          // Initially, keyboard RGB is enabled. Change to false config.h initializes RGB disabled.
};

void keyboard_post_init_user(void) {
    rgb_matrix_enable();
    layer_on(_AMBER_LAYER);

    mac_mode = false;

    if (mac_mode) {
        blink_key(mac_mode_index, 3, mac_mode_blink_color);
    } else {
        blink_key(pc_mode_index, 3, mac_mode_blink_color);
    }
}

#define MODS_SHIFT  (get_mods() & MOD_BIT(KC_LSHIFT) || get_mods() & MOD_BIT(KC_RSHIFT))
#define MODS_CTRL  (get_mods() & MOD_BIT(KC_LCTL) || get_mods() & MOD_BIT(KC_RCTRL))
#define MODS_ALT  (get_mods() & MOD_BIT(KC_LALT) || get_mods() & MOD_BIT(KC_RALT))

bool handle_partial_layer(
        // Current event
        uint16_t event_keycode,
        keyrecord_t *record,

        // Keycode to handle and emit
        uint16_t layer_trigger_keycode,
        uint16_t layer_emit_keycode,

        // Layer to use for partial layer
        uint16_t layer,

        // Static fields (must be unique to this layer)
        bool *disable_tap,
        uint32_t *key_timer) {
    if (event_keycode == layer_trigger_keycode) {
        // When first pressed, allow tap event and time from now
        if (record->event.pressed) {
            *key_timer = timer_read32();
            *disable_tap = false;
            layer_on(layer);

        // when released, check timer to see if we allow tap
        } else {
            layer_off(layer);

            if (!*disable_tap && timer_elapsed32(*key_timer) < 500) {
                tap_code(layer_emit_keycode);
            }
        }

        return false;
    }

    // Special handling of transparent keys
    if (IS_LAYER_ON(layer)) {
        *disable_tap = true;

        const keypos_t key = record->event.key;
        const uint16_t keymap_value = keymaps[layer][key.row][key.col];

        if (keymap_value == KC_TRNS) {
            if(record->event.pressed) {
                register_code(layer_emit_keycode);
                tap_code(event_keycode);
                unregister_code(layer_emit_keycode);
            }
            return false;
        }
    }

    // Continue processing
    return true;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    static uint32_t boot_key_timer;

    static bool alt_disable_tap;
    static uint32_t alt_key_timer;

    static bool caps_disable_tap;
    static uint32_t caps_key_timer;

    handle_partial_layer(keycode, record, AD_LALT, KC_LALT, _ALT_LAYER, &alt_disable_tap, &alt_key_timer);
    handle_partial_layer(keycode, record, AD_CAPS, KC_CAPS, _CAPS_LAYER, &caps_disable_tap, &caps_key_timer);

    switch (keycode) {
        case KC_GESC:
            if (record->event.pressed && MODS_CTRL) {
                // Ctrl + ` hacks for VSCode
                if (!MODS_SHIFT) {
                    SEND_STRING("`");
                } else {
                // Still let ctrl + shift + esc work for task manager
                    register_code(KC_LCTL);
                    register_code(KC_LSHIFT);
                    tap_code(KC_ESC);
                    unregister_code(KC_LCTL);
                    unregister_code(KC_LSHIFT);
                }

                return false;
            }
            return true;

        case AD_UASH:
            if(record->event.pressed) {
                send_alt_code(198);
            }
            return false;

        case AD_LASH:
            if(record->event.pressed) {
                send_alt_code(230);
            }
            return false;

        case AD_DOT:
            if(record->event.pressed) {
                if (!mac_mode) {
                    send_alt_code(7);
                } else {
                    register_code(KC_LALT);
                    tap_code(KC_8);
                    unregister_code(KC_LALT);
                }
            }
            return false;

        case AD_LOCK:
            if (record -> event.pressed) {
                // Lock device
                if (MODS_SHIFT) {
                    unregister_code(KC_RSHIFT);
                }

                register_code(KC_LGUI);
                tap_code(KC_L);
                unregister_code(KC_LGUI);

                if (MODS_SHIFT) {
                    register_code(KC_RSHIFT);
                }
            } else {
                if (MODS_SHIFT) {
                    layer_on(_LOCK_LAYER);
                }
            }
            return false;

        case AD_CTRL:
            if (mac_mode) {
                if (record -> event.pressed) {
                    register_code(KC_LGUI);
                } else {
                    unregister_code(KC_LGUI);
                }
            } else {
                if (record -> event.pressed) {
                    register_code(KC_LCTL);
                } else {
                    unregister_code(KC_LCTL);
                }
            }
            return false;

        case AD_LGUI:
            if (mac_mode) {
                if (record -> event.pressed) {
                    register_code(KC_LCTL);
                } else {
                    unregister_code(KC_LCTL);
                }
            } else {
                if (record -> event.pressed) {
                    register_code(KC_LGUI);
                } else {
                    unregister_code(KC_LGUI);
                }
            }
            return false;

        case AD_MODE:
            if (record -> event.pressed) {
                mac_mode = !mac_mode;

                if (mac_mode) {
                    blink_key(mac_mode_index, 3, mac_mode_blink_color);
                } else {
                    blink_key(pc_mode_index, 3, mac_mode_blink_color);
                }
            }
            return false;

        case U_T_AUTO:
            if (record->event.pressed && MODS_SHIFT && MODS_CTRL) {
                TOGGLE_FLAG_AND_PRINT(usb_extra_manual, "USB extra port manual mode");
            }
            return false;
        case U_T_AGCR:
            if (record->event.pressed && MODS_SHIFT && MODS_CTRL) {
                TOGGLE_FLAG_AND_PRINT(usb_gcr_auto, "USB GCR auto mode");
            }
            return false;
        case DBG_TOG:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_enable, "Debug mode");
            }
            return false;
        case DBG_MTRX:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_matrix, "Debug matrix");
            }
            return false;
        case DBG_KBD:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_keyboard, "Debug keyboard");
            }
            return false;
        case DBG_MOU:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_mouse, "Debug mouse");
            }
            return false;
        case MD_BOOT:
            if (record->event.pressed) {
                boot_key_timer = timer_read32();
            } else {
                if (timer_elapsed32(boot_key_timer) >= 500) {
                    reset_keyboard();
                }
            }
            return false;
        case RGB_TOG:
            if (record->event.pressed) {
                switch (rgb_matrix_get_flags()) {
                    case LED_FLAG_ALL: {
                        rgb_matrix_set_flags(LED_FLAG_KEYLIGHT | LED_FLAG_MODIFIER);
                        rgb_matrix_set_color_all(0, 0, 0);
                    }
                    break;
                    case LED_FLAG_KEYLIGHT | LED_FLAG_MODIFIER: {
                        rgb_matrix_set_flags(LED_FLAG_UNDERGLOW);
                        rgb_matrix_set_color_all(0, 0, 0);
                    }
                    break;
                    case LED_FLAG_UNDERGLOW: {
                        rgb_matrix_set_flags(LED_FLAG_NONE);
                        rgb_matrix_disable_noeeprom();
                    }
                    break;
                    default: {
                        rgb_matrix_set_flags(LED_FLAG_ALL);
                        rgb_matrix_enable_noeeprom();
                    }
                    break;
                }
                return false;
            }
        default:
            break; //Process all other keycodes normally
    }

    return true;
}

void rgb_matrix_indicators_user(void) {
    if (disable_layer_color ||
        rgb_matrix_get_flags() == LED_FLAG_NONE ||
        rgb_matrix_get_flags() == LED_FLAG_UNDERGLOW) {
            return;
        }

    set_layer_color(get_highest_layer(layer_state));
}

const uint16_t NUMPAD_CODES[] = {
    KC_KP_0,
    KC_KP_1,
    KC_KP_2,
    KC_KP_3,
    KC_KP_4,
    KC_KP_5,
    KC_KP_6,
    KC_KP_7,
    KC_KP_8,
    KC_KP_9,
};

void send_alt_code(int code) {
    // Don't do this on mac
    if (mac_mode) return;

    bool numlock_enabled = host_keyboard_led_state().num_lock;
    if (!numlock_enabled) {
        register_code(KC_NUMLOCK);
        unregister_code(KC_NUMLOCK);
    }

    short digit1 = code / 1000 % 10;
    short digit2 = code / 100  % 10;
    short digit3 = code / 10   % 10;
    short digit4 = code        % 10;

    register_code(KC_LALT);
    if (code > 999)
        tap_code(NUMPAD_CODES[digit1]);

    if (code > 99)
        tap_code(NUMPAD_CODES[digit2]);

    if (code > 9)
        tap_code(NUMPAD_CODES[digit3]);

    tap_code(NUMPAD_CODES[digit4]);
    unregister_code(KC_LALT);

    if (!numlock_enabled) {
        register_code(KC_NUMLOCK);
        unregister_code(KC_NUMLOCK);
    }
}
