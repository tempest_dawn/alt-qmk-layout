from ubuntu:22.04

run apt-get update && apt-get install -y \
  sudo \
  git \
  python3-pip \
  dos2unix \
  curl \
  gcc-arm-none-eabi

# Clone firmware ourselves
workdir /qmk_firmware
run git clone https://github.com/qmk/qmk_firmware /qmk_firmware
run git checkout 0.18.17
run git submodule init
run git submodule update
run /qmk_firmware/util/install/debian.sh
run make git-submodule

run python3 -m pip install --user qmk

env PATH="/root/.local/bin:${PATH}"
env QMK_FIRMWARE_DIR=/qmk_firmware

# run qmk setup -y -H /qmk_firmware

copy /scripts/compile.sh /opt/compile.sh
run dos2unix /opt/compile.sh
run chmod +x /opt/compile.sh
copy ./keymap/* /qmk_firmware/keyboards/massdrop/alt/keymaps/ashe/

run find /qmk_firmware/keyboards/massdrop/alt/keymaps/ashe | xargs dos2unix

cmd /opt/compile.sh
