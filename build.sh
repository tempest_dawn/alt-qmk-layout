#!/bin/bash
set -e

mkdir -p out

IMAGE=$(docker build -q .)

docker run -it \
  --mount type=bind,src=$(pwd)/out,dst=/out \
  $IMAGE
