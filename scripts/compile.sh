#!/bin/bash
set -e

cd /qmk_firmware
qmk compile -kb massdrop/alt -km ashe
cp massdrop_alt_ashe.bin /out
